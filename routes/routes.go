package routes

import (
	"fmt"
	"log"
	"myapp/controller"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func InitializeRoutes() {
	muxRouter := mux.NewRouter()

	// student routes
	muxRouter.HandleFunc("/home", controller.HomeHandler)
	muxRouter.HandleFunc("/home/{myname}", controller.ParameterHandler)
	muxRouter.HandleFunc("/student", controller.AddStudent).Methods("POST")
	muxRouter.HandleFunc("/student/{stdid}", controller.GetStud).Methods("GET")
	muxRouter.HandleFunc("/student/{sid}", controller.UpdateStud).Methods("PUT")
	muxRouter.HandleFunc("/student/{sid}", controller.DeleteStud).Methods("DELETE")
	muxRouter.HandleFunc("/students", controller.GetAllStuds)

	//router for the addCourse function in course
	muxRouter.HandleFunc("/course", controller.AddCourse).Methods("POST")

	// route for the data for the read function
	muxRouter.HandleFunc("/course/{cid}", controller.GetCourse).Methods("GET")

	// router for the update function
	muxRouter.HandleFunc("/course/{cid}", controller.UpdateCourse).Methods("PUT")

	// router for the delete function
	muxRouter.HandleFunc("/course/{cid}", controller.DeleteCourse).Methods("DELETE")

	// router for the read function to get all the courses
	muxRouter.HandleFunc("/courses", controller.ShowAllCourses).Methods("GET")

	//Admin routes
	muxRouter.HandleFunc("/signup", controller.Signup).Methods("POST")
	muxRouter.HandleFunc("/login", controller.Login).Methods("POST")
	muxRouter.HandleFunc("/logout", controller.Logout).Methods("GET")

	//Enroll
	muxRouter.HandleFunc("/enroll", controller.Enroll).Methods("POST")
	muxRouter.HandleFunc("/enroll/{sid}/{cid}", controller.GetEnroll).Methods("GET")
	muxRouter.HandleFunc("/enrolls", controller.GetEnrolls).Methods("GET")

	// Serving static file
	fhandler := http.FileServer(http.Dir("./view"))
	muxRouter.PathPrefix("/").Handler(fhandler)

	err := http.ListenAndServe(":8081", muxRouter)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	log.Println("Application running on port 8081")
}
