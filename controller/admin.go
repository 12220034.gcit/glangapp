package controller

import (
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"
	"time"
)

func Signup(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&admin); err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, "invalid json body")
		return
	}

	defer r.Body.Close()
	saveErr := admin.Create()
	if saveErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, saveErr.Error())
		return
	}
	// no error
	httpResp.RespondwithJSON(w, http.StatusCreated, map[string]string{"status": "admin added"})
}

func Login(w http.ResponseWriter, r *http.Request) {
	var admin model.Admin
	err := json.NewDecoder(r.Body).Decode(&admin)

	if err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, "invalid json body")
		return
	}
	defer r.Body.Close()

	getErr := admin.Get()

	if getErr != nil {
		httpResp.RespondwithError(w, http.StatusUnauthorized, getErr.Error())
		return
	}

	cookie := http.Cookie{
		Name:    "my-cookie",
		Value:   "my-value",
		Expires: time.Now().Add(30 * time.Minute),
		Secure:  true,
	}
	http.SetCookie(w, &cookie)
	httpResp.RespondwithJSON(w, http.StatusOK, map[string]string{"message": "success"})
}

func Logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:    "my-cookie",
		Expires: time.Now(),
	})
	httpResp.RespondwithJSON(w, http.StatusOK, map[string]string{"message": "Log out success"})
}

func VerifyCookie(w http.ResponseWriter, r *http.Request) bool {
	// check cookie in request
	cookie, err := r.Cookie("my-cookie")
	if err != nil {
		if err == http.ErrNoCookie {
			httpResp.RespondwithError(w, http.StatusUnauthorized, "cookie not found")
			return false
		}
		httpResp.RespondwithJSON(w, http.StatusInternalServerError, "InternalServerError")
		return false
	}
	// No error
	if cookie.Value != "my-value" {
		httpResp.RespondwithError(w, http.StatusUnauthorized, "cookie value does not match")
		return false
	}
	return true
}
