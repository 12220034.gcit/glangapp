package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/date"
	"myapp/utils/httpResp"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

func Enroll(w http.ResponseWriter, r *http.Request) {
	var e model.Enroll

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&e)
	if err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, "message: invalid json body")
		return
	}
	e.Date_Enrolled = date.GetDate()

	saveErr := e.EnrollStud()
	if saveErr != nil {
		if strings.Contains(saveErr.Error(), "duplicate key") {
			httpResp.RespondwithError(w, http.StatusForbidden, "student already enrolled.")
			return
		}
		httpResp.RespondwithError(w, http.StatusInternalServerError, saveErr.Error())
		return
	}
	//No Error
	httpResp.RespondwithJSON(w, http.StatusCreated, map[string]string{"status": "enrolled"})
}

func GetEnroll(w http.ResponseWriter, r *http.Request) {

	sid := mux.Vars(r)["sid"]
	cid := mux.Vars(r)["cid"]

	stdid, _ := strconv.ParseInt(sid, 10, 64)

	e := model.Enroll{Stdd: int(stdid), CourseId: cid}

	getErr := e.Get()
	if getErr != nil {
		switch getErr {
		case sql.ErrNoRows:
			httpResp.RespondwithError(w, http.StatusNotFound, "No such enrollments")
		default:
			httpResp.RespondwithError(w, http.StatusInternalServerError, getErr.Error())
		}
		return
	}
	httpResp.RespondwithJSON(w, http.StatusOK, e)
}
func GetEnrolls(w http.ResponseWriter, r *http.Request) {
	enrolls, getErr := model.GetAllEnrolls()
	if getErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, getErr.Error())
		return
	}
	httpResp.RespondwithJSON(w, http.StatusOK, enrolls)
}

func DeleteEnroll(w http.ResponseWriter, r *http.Request) {
	sid := mux.Vars(r)["sid"]
	cid := mux.Vars(r)["cid"]
	stdid, _ := strconv.ParseInt(sid, 10, 64)

	e := model.Enroll{Stdd: int(stdid), CourseId: cid}

	if err := e.Delete(); err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondwithJSON(w, http.StatusOK, map[string]string{"status": "deleted"})

}
