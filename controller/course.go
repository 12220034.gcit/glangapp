package controller

import (
	"database/sql"
	"encoding/json"
	"myapp/model"
	"myapp/utils/httpResp"
	"net/http"

	"github.com/gorilla/mux"
)

func AddCourse(w http.ResponseWriter, r *http.Request) {
	//create variable of type course
	var course model.Course

	//read the request body and create a decoder object
	decoder := json.NewDecoder(r.Body) //allows to decode json data from the request body

	//store the json object data
	if err := decoder.Decode(&course); err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, "Invalid json body")
	}
	//defer the closing request body until the function returns
	defer r.Body.Close()

	//call the Add() using Course object, course
	saveErr := course.Add()

	if saveErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, saveErr.Error())
		return
	} else {
		httpResp.RespondwithJSON(w, http.StatusCreated, map[string]string{"status": "course added"})
	}
}

func GetCourse(w http.ResponseWriter, r *http.Request) {
	//type string
	getCidFrmURL := mux.Vars(r)["cid"]

	course := model.Course{Cid: getCidFrmURL}

	courseErr := course.Show()

	if courseErr != nil {
		switch courseErr {
		case sql.ErrNoRows:
			httpResp.RespondwithError(w, http.StatusNotFound, "Course not in the table")
		default:
			httpResp.RespondwithError(w, http.StatusInternalServerError, courseErr.Error())
		}
	} else {
		httpResp.RespondwithJSON(w, http.StatusOK, course)
	}
}

func UpdateCourse(w http.ResponseWriter, r *http.Request) {
	old_cid := mux.Vars(r)["cid"]
	var cid model.Course

	updateErr := cid.Update(old_cid)

	if updateErr != nil {
		switch updateErr {
		case sql.ErrNoRows:
			httpResp.RespondwithError(w, http.StatusNotFound, "Course not found")
		default:
			httpResp.RespondwithError(w, http.StatusInternalServerError, updateErr.Error())
		}
		httpResp.RespondwithJSON(w, http.StatusOK, cid)
	}

	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&cid); err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, "Invalid json")
		return
	}
	defer r.Body.Close()

	err := cid.Update(old_cid)

	if err != nil {
		httpResp.RespondwithError(w, http.StatusInternalServerError, err.Error())
	}
	httpResp.RespondwithJSON(w, http.StatusOK, cid)

}

func DeleteCourse(w http.ResponseWriter, r *http.Request) {
	cid := mux.Vars(r)["cid"]

	var idErr error
	if idErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, idErr.Error())
		return
	}

	course_id := model.Course{Cid: cid}
	if err := course_id.Delete(); err != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, err.Error())
		return
	}
	httpResp.RespondwithJSON(w, http.StatusOK, map[string]string{"Status": "Deleted"})
}

func ShowAllCourses(w http.ResponseWriter, r *http.Request) {
	courses, getErr := model.GetAllCourses()

	if getErr != nil {
		httpResp.RespondwithError(w, http.StatusBadRequest, getErr.Error())
	}
	httpResp.RespondwithJSON(w, http.StatusOK, courses)
}
