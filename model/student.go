package model

import (
	"myapp/dataStore/postgres"
)

type Student struct {
	StdId     int64  `json:"stdid"`
	FirstName string `json:"fname"`
	LastName  string `json:"lname"`
	Email     string `json:"email"`
}

const (
	queryInsert     = `INSERT INTO student (stdid, firstname, lastname, email) VALUES ($1, $2, $3, $4);`
	queryGetUser    = `SELECT stdid, firstname, lastname, email FROM student WHERE stdid=$1 ;`
	queryUpdate     = `UPDATE student SET stdid=$1, firstname=$2, lastname=$3, email=$4 WHERE stdid=$5 RETURNING stdid;`
	queryDeleteUser = `DELETE FROM student WHERE stdid=$1 RETURNING stdid;`
)

func (s *Student) Create() error {
	_, err := postgres.Db.Exec(queryInsert, s.StdId, s.FirstName, s.LastName, s.Email)
	return err
}

func (s *Student) Read() error {
	// postgres.Db.QueryRow(queryGetUser, s.StdId).Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)
	row := postgres.Db.QueryRow(queryGetUser, s.StdId)
	err := row.Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)
	// fmt.Println(row)
	return err
}

func (s *Student) Update(oldID int64) error {
	row := postgres.Db.QueryRow(queryUpdate, s.StdId, s.FirstName, s.LastName, s.Email, oldID)
	err := row.Scan(&s.StdId)
	return err
}

func (s *Student) Delete() error {
	err := postgres.Db.QueryRow(queryDeleteUser, s.StdId).Scan(&s.StdId)
	return err
}

func GetAllStuds() ([]Student, error) {
	rows, getErr := postgres.Db.Query("Select * FROM student;")
	if getErr != nil {
		return nil, getErr
	}
	students := []Student{}

	for rows.Next() {
		var s Student
		dbErr := rows.Scan(&s.StdId, &s.FirstName, &s.LastName, &s.Email)
		if dbErr != nil {
			return nil, dbErr
		}
		students = append(students, s)
	}
	rows.Close()
	return students, nil
}
