window.onload = function () {
    fetch("/courses")
    .then((response) => response.text())
    .then((data) => showCourses(data))
}

function addCourse(){
    var data = getFormValue()
    var Cid = data.cid

    if (Cid == ""){
        alert("Course ID cannot be empty")
        return
    }else if (data.Coursename == ""){
        alert("Course Name cannot be empty")
        return
    }

    fetch("/course", {
        method:"POST",
        body: JSON.stringify(data),
        headers: { "Content-type": "application/json; charset=UTF-8"},
    })
    .then((response1) => {
        if (response1.ok){
            fetch("/course/" + Cid)
            .then((response2)=> response2.text())
            .then((data) => showCourse(data));
        }else{
            throw new Error(response1.statusText);
        }
    })
    .catch((e) => {
        alert(e)
    });
    resetform();
}

function showCourse(data){
    const course = JSON.parse(data);
    createNewRow(course)
}
// helper function to create new row 
function createNewRow(course){
    
    var table = document.getElementById("CourseTable");

    var row = table.insertRow(table.rows.length);

    var td = [];

    for (i = 0; i<table.rows[0].cells.length; i++){
        td[i] = row.insertCell(i);
    }
    td[0].innerHTML = course.cid;
    td[1].innerHTML = course.cname;
    td[2].innerHTML =
      '<input type = "button" onclick="deleteCourse(this)" value="delete" id="button-1">';
    td[3].innerHTML =
      '<input type = "button" onclick="updateCourse(this)" value = "edit" id= "button-2">';
}

function resetform(){
    document.getElementById("cid").value = "";
    document.getElementById("cname").value = "";
}

function showCourses(data){
    const courses = JSON.parse(data)
    courses.forEach((course) => {
        createNewRow(course)
    });
}


//display  the data in the form 
var selectRow = null

//helper function get form value
function getFormValue() {
    var formValue = {
        cid: document.getElementById("cid").value,
        cname: document.getElementById("cname").value,
    }
    return formValue
}

function updateCourse(r){
    selectRow = r.parentElement.parentElement;

    //fill in the form field with the selected row data
    document.getElementById("cid").value = selectRow.cells[0].innerHTML
    document.getElementById("cname").value = selectRow.cells[1].innerHTML

    var btn = document.getElementById("add-button")
    cid = selectRow.cells[0].innerHTML
    if(btn){
        btn.innerHTML = "Update"
        btn.setAttribute("onclick", "update(cid)")
    }
}

//send update http request 

function update(cid){
    var newData = getFormValue()
    fetch("/course/" + cid, {
        method: "PUT",
        body: JSON.stringify(newData),
        headers: {"Content-type":"application/json; charset=UTF-8"}
    }).then(res => {
        if (res.ok){
            //fills the selected row with new value
            selectRow.cells[0].innerHTML = newData.cid;
            selectRow.cells[1].innerHTML = newData.cname;

            //set to previous value
            var button = document.getElementById("add-button");
            button.innerHTML = "Add";
            button.setAttribute("onclick", "addCourse()")
            selectRow = null 
            resetform()

        }else{
            alert("Server: Update request error.")
        }
    })
}

function deleteCourse(r) {
    if (confirm("Are you sure want to Delete this?")) {
        selectRow = r.parentElement.parentElement;
        cid = selectRow.cells[0].innerHTML;

        fetch("/course/" + cid, {
            method: "DELETE",
            headers: { "Content-type": "application/json; charset=UTF-8" },
        })
            .then((response) => {
                if (response.ok) {
                    var rowIndex = selectRow.rowIndex;
                    if (rowIndex > 0) {
                        document.getElementById("CourseTable").deleteRow(rowIndex);
                    }
                    selectRow = null;
                }
            })
            .catch((error) => {
                console.error("Error:", error);
            });
    }
}
